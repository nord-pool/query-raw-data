import urllib.request
import json
import datetime
from time import sleep

# todo: override
# todo: data type


class RawDataQuery(object):
    def run_intradate(self, folder_path, start_date_str, end_date_str):
        inter_day_base_name = "interdate"
        missing_dates = self.find_missing_dates(start_date_str, end_date_str, folder_path + inter_day_base_name)
        # todo: can load and save be refactored to chained maps in python3?
        self.load_and_save_date_static(missing_dates, folder_path + inter_day_base_name)

    @staticmethod
    def load_and_save_date_static(missing_dates, static_file_path):
        for missing_date in missing_dates:
            with urllib.request.urlopen("http://www.nordpoolspot.com/api/marketdata/page/194?currency=,EUR,EUR,EUR&endDate=" + missing_date + "&entityName=FI") as url:
                s = url.read().decode("utf-8")

            with open(static_file_path + missing_date + '.json', 'w') as outfile:
                json.dump(s, outfile)

            print("downloaded date " + missing_date)
            # nord-pool is timeouting request every now and then. Sleep is to test if those time outs can  be made rarer
            # if not requesting so often.
            sleep(3)

    @staticmethod
    def find_missing_dates(start_date_str, end_date_str, static_file_path):
        def exists(file_full_name):
            try:
                open(file_full_name)
                return True
            except IOError:
                return False

        def convert_to_dates(start_date_str, end_date_str):
            start_date = datetime.datetime.strptime(start_date_str, '%d-%m-%Y')
            end_date = datetime.datetime.strptime(end_date_str, '%d-%m-%Y')

            if start_date.date() > end_date.date():
                raise Exception("start date is after end date")

            return start_date, end_date

        def calibrate_between_limits(start_date, end_date):
            records_since = datetime.datetime(2014, 11, 25)
            calibrated_start = start_date
            calibrated_end = end_date

            # todo: should be today - 1?
            # todo: make some solution for timezone
            if end_date.date() > datetime.datetime.now(datetime.timezone.utc).date():
                calibrated_end = datetime.datetime.now(datetime.timezone.utc)

            if start_date.date() < records_since.date():
                calibrated_start = records_since

            return calibrated_start, calibrated_end

        def missing_dates(start_date, end_date):
            missing_files = []
            current_date = start_date
            while current_date.date() <= end_date.date():
                if exists(static_file_path + current_date.strftime('%d-%m-%Y') + ".json") == False:
                    missing_files.append(current_date .strftime('%d-%m-%Y'))
                current_date = current_date + datetime.timedelta(days=1)
            return missing_files

        start_date, end_date = convert_to_dates(start_date_str, end_date_str)
        start_date, end_date = calibrate_between_limits(start_date, end_date)
        return missing_dates(start_date, end_date)



