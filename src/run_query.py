from src.raw_data_query import RawDataQuery

folderPath = "../data/"
startDateStr = "23-11-2014"
endDateStr = "04-12-2014"

rtq = RawDataQuery()
rtq.run_intradate(folderPath, startDateStr, endDateStr)
